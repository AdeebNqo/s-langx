import sys
try:
	error = 0
	filename1 = sys.argv[1]
	filename2 = sys.argv[2]
	file1 = open(filename1,'r')
	file2 = open(filename2,'r')
	file1lines = file1.readlines()
	file2lines = file2.readlines()
	for i in range(len(file1lines)):
		if (file1lines[i] != file2lines[i]):
			print("Error: line "+str(i))
			error = 1
			break;
	if (error == 0):
		print("Congrats, output is the same")
except IndexError:
	print("Error: check number of param")
