#
#	
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
# 
# @author Zola 'adeebnqo' Mahlaza
# @version 1.0
# @quote
#	"Clothes make the man. Naked people have little or no influence on society."
#
#	-Mark Twain

import lex
import sys


literals = "(}{)[/]+-%;,.*="
#tokens with values to be printed within brackets
types_print = [
	'INT_LITERAL',
	'ID',
	'FLOAT_LITERAL'
]
#reserved words
reserved={
	'void':'VOID',
	'for':'FOR',
	'if':'IF',
	'float':'FLOAT',
	'int':'INT',
	'return':'RETURN',
	'else':'ELSE'
}
#compound operators
additional_op = [
	'<=',
	'>=',
	'==',
	'!=',
	'&&',
	'||',
	'()',
	'[]',
	'{}'
]
#more tokens
tokens = [
	'ID',
	'INT_LITERAL',
	'FLOAT_LITERAL',
	'WHITESPACE',
	'COMMENT',
	'LARROW',
	'RARROW',
	'DEQUAL', #Double equal ==
	'NEQUAL', #Not equal
	'DAND', #Double and &&
	'OR',
	'DRBRACE', #Double Round brace
	'DSBRACE', #Double Square brace
	'DCBRACE'  #Double Curly brace
] + list(reserved.values())
def main():
	try:	
		input_data = ""
		input_file = sys.argv[1]
		input_stream = open(input_file)
		for line in input_stream.readlines():
			input_data+=line
		#regular expressions for the tokens
		t_COMMENT = r'(\/\/(.+)$)|(\/\*(\s|.)+\*\/)'
		t_WHITESPACE = r'\s+'
		t_INT_LITERAL = r'-?\d+'
		t_FLOAT_LITERAL = r'-?\d+\.(\d+((e-|E-|E+|E-)\d+)?(f|F)?)?' #r'((\d+)\.(\d+)e-(\d+)(F|f))|((\d+)\.(\d+)f)|(\d+\.\d+)|(\.(\d+))|(\d+\.)'
		t_LARROW = r'<='
		t_RARROW = r'>='
		t_DEQUAL = r'=='
		t_NEQUAL = r'!='
		t_DAND = r'&&'
		t_OR = r'\|\|'
		t_DRBRACE = r'\(\)'
		t_DSBRACE = r'\[]'
		t_DCBRACE = r'{}'
		#creating an output file
		output_file = open(input_file.replace('.sl','.out'),'w')
		#creating a lexer
		analyzer = lex.lex()
		analyzer.input(input_data)
		for token in analyzer:
			if (token.value in additional_op):
				output_file.write(token.value+'\n')
			else:
				output_file.write(token.type)
				if token.type in types_print:
					output_file.write('('+token.value+')')
				output_file.write('\n')
	except IndexError:
		print("Invalid number of parameters.\nPlease run code with 1 input file, eg `python main.py sample.in`")
	except IOError:
		print("IOError, please ensure file name is correct")

def t_error(token):
	print("Illegal character '%s'" % token.value[0])
	token.lexer.skip(1)

def t_ID(t):
	r'[a-zA-Z_][a-zA-Z0-9_]*'
	#r'(_|[a-zA-Z])\w+'
	#print("t.value is "+str(t.value))
	if t.value in reserved:
		t.type = reserved[ t.value ]
	return t
if __name__=='__main__':
	main()
